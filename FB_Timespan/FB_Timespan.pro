#-------------------------------------------------
#
# Project created by QtCreator 2016-03-24T20:29:00
#
#-------------------------------------------------

QT       -= gui

TARGET = FB_Timespan
TEMPLATE = lib

DEFINES += FB_TIMESPAN_LIBRARY

SOURCES += fb_timespan.cpp

HEADERS += fb_timespan.h\
        fb_timespan_global.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
