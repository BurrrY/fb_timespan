#-------------------------------------------------
#
# Project created by QtCreator 2016-04-01T12:59:29
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = tst_fb_timespan_test
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += tst_fb_timespan_test.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

win32:LIBS += -L$$DESTDIR/ -lFB_Timespan
else:unix:!macx: LIBS += -L$$DESTDIR/ -lFB_Timespan

INCLUDEPATH += $$PWD/../FB_Timespan
DEPENDPATH += $$PWD/../FB_Timespan


